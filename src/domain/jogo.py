class Jogo:
    def __init__(self, nome, categoria, console, id=None, capa=None):
        self.__id = id
        self.__nome = nome
        self.__categoria = categoria
        self.__console = console
        self.capa = capa

    @property
    def nome(self):
        return self.__nome

    @nome.setter
    def nome(self, nome):
        self.__nome = nome

    @property
    def categoria(self):
        return self.__categoria

    @categoria.setter
    def categoria(self, categoria):
        self.__categoria = categoria

    @property
    def console(self):
        return self.__console

    @console.setter
    def console(self, console):
        self.__console = console

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, id):
        self.__id = id