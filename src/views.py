from flask import render_template, request, redirect, session, flash, url_for, send_from_directory
import base64

from src.dao.jogo_dao import JogoDao
from src.dao.usuario_dao import UsuarioDao
from src.domain.jogo import Jogo

from jogoteca import db, app

jogo_dao = JogoDao(db)
usuario_dao =UsuarioDao(db)

@app.route('/ola')
def ola():
    return '<h1>Hello world!</h1>'

@app.route('/')
def index():
    lista = jogo_dao.listar()
    return render_template('lista.html', titulo='Jogos', jogos=lista)


def _sessao_vazia():
    return 'usuario_logado' not in session or session['usuario_logado'] == None

@app.route('/novo')
def novo():
    if _sessao_vazia():
        return redirect(url_for('login', proxima='novo'))

    return render_template('novo.html', titulo='Novo jogo')

@app.route('/criar', methods=['POST'],)
def criar():
    nome = request.form['nome']
    categoria = request.form['categoria']
    console = request.form['console']

    arquivo = request.files['arquivo']

    encoded_string = base64.b64encode(arquivo.read())

    jogo = Jogo(nome, categoria, console, capa='fail')
    jogo.capa=encoded_string

    jogo_dao.salvar(jogo)

    return redirect(url_for('index'))

@app.route('/editar/<int:id>')
def editar(id):
    if _sessao_vazia():
        return redirect(url_for('login', proxima='editar'))

    jogo = jogo_dao.busca_por_id(id)
    return render_template('editar.html', titulo='Editar', jogo=jogo, capa_jogo=jogo.capa)

@app.route('/atualizar', methods=['POST'],)
def atualizar():
    id = request.form['id']
    nome = request.form['nome']
    categoria = request.form['categoria']
    console = request.form['console']

    arquivo = request.files['arquivo']

    encoded_string = base64.b64encode(arquivo.read())

    jogo = Jogo(nome, categoria, console)
    jogo.capa=encoded_string
    jogo.id = id

    jogo_dao.salvar(jogo)

    return redirect(url_for('index'))

@app.route('/apagar/<int:id>')
def apagar(id):
    if _sessao_vazia():
        return redirect(url_for('login', proxima='editar'))

    jogo_dao.deletar(id)
    flash('O jogo foi removido com sucesso')
    return redirect(url_for('index'))




@app.route('/login')
def login():
    proxima = request.args.get('proxima')
    return render_template('login.html', titulo='Login', proxima=proxima)

@app.route('/autenticar', methods=['POST'],)
def autenticar():
    usuario = usuario_dao.buscar_por_id(request.form['usuario'])

    if usuario:
        if usuario.senha == request.form['senha']:
            session['usuario_logado'] = usuario.id
            flash(usuario.nome + " logou com sucesso")

            proxima_pagina = request.form['proxima']
            print(proxima_pagina)
            if (not proxima_pagina or proxima_pagina == 'None'):
                proxima_pagina = 'index'
            print(proxima_pagina)
            return redirect(url_for(proxima_pagina))

    else:
        flash("Usuário inválido")
        return redirect(url_for('login'))

@app.route('/sair')
def sair():
    session['usuario_logado'] = None
    flash("Nenhum usuário logado")
    return redirect(url_for('index'))

@app.route('/uploads/<nome_arquivo>')
def imagem(nome_arquivo):
    return send_from_directory('uploads', nome_arquivo)